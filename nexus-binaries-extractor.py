#!/usr/bin/env python

"""This utility automates running Android Binaries for Nexus Devices scripts"""

# Copyright (c) 2016 Ruslan Mstoi <ruslan.mstoi@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import pexpect

def exec_script(script, logfile):
    """Run Nexus binaries shell script"""
    child = pexpect.spawn('./' + script)

    child.logfile = logfile

    child.expect('Press Enter to view the license')

    # send Ctrl-J = Enter
    child.sendcontrol('j')

    child.expect('--More--')
    child.send('q')

    child.expect('Type "I ACCEPT" if you agree to the terms of the license:')
    child.sendline('I ACCEPT')

    child.expect(pexpect.EOF, timeout=None)

def main():
    """Simply main"""
    parser = argparse.ArgumentParser(
        description='Auto runner of Binaries for Nexus Devices shell scripts.')
    parser.add_argument('binaries_sh_scripts', nargs='+',
                        help='List of Binaries for Nexus Devices shell '
                        'scripts. Multiple scripts can be scpecified using '
                        'globbing.')
    args = parser.parse_args()

    print "About to run scripts:", args.binaries_sh_scripts

    logfile_name = "binaries_scripts.log"
    logfile = open(logfile_name, "w")

    print "Check", logfile_name, "for details\n"

    for script in args.binaries_sh_scripts:
        print "Running", script
        exec_script(script, logfile)

    print "Bye!"

if __name__ == "__main__":
    main()
